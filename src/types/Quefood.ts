import type OrderItem from "./OrderItem";

export default interface QueFood {
  id?: number;
  status?: string;

  orderItems?: OrderItem; // FK
  orderitemid?: number; //เพิ่ม
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
