export default interface Stock {
  id?: number;
  nameEmp: string;
  createdDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
