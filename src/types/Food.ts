export default interface Food {
  id?: number;
  name: string;
  price: number;
  image?: string;
  categoryId: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;

  /*  catagory: string;
  img: string;
  qty: number;  */
}
