import type Food from "./Food";
import type Order from "./Order";
import type Quefood from "./Quefood";
export default interface OrderItem {
  id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  order?: Order;
  food?: Food; // Food Id
  quefood?: Quefood[]; //Que id
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
