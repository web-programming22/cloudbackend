import type Material from "./Material";
import type Stock from "./Stock";

export default interface StockItem {
  id?: number;
  nameMaterial?: string;
  amount?: number;
  price?: number;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
  stock?: Stock;
  material?: Material;
}
