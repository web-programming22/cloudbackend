import type Material from "./Material";
import type Receipt from "./Receipt";

export default interface ReceiptItem {
  id?: number;
  name?: string;
  amount: number; // จำนวน
  price: number; // ราคาต่อชิ้น
  total?: number; // ราคารวม
  createdDate?: Date; // วันที่สร้าง
  updatedDate?: Date;
  deletedDate?: Date;
  receipt?: Receipt;
  material?: Material;
  //material?: number;
}
