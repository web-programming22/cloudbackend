export default interface QueueSaveOrderItem {
  id?: number;
  status?: string;
  orderitem?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
