import type Stock from "@/types/Stock";
import http from "./axios";
function getStock() {
  return http.get("/stocks");
}

function saveStock(stocks: Stock) {
  return http.post("/stocks", stocks);
}

function updateStock(id: number, stocks: Stock) {
  return http.patch(`/stocks/${id}`, stocks);
}

function deleteStock(id: number) {
  return http.delete(`/stocks/${id}`);
}

export default { getStock, saveStock, updateStock, deleteStock };
