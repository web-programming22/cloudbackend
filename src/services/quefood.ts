import type QueFood from "@/types/Quefood";
import http from "./axios";

function getQuefoods() {
  return http.get("/quesfoods");
}
function getQuefoodsID(id: number) {
  return http.get(`/quesfoods/${id}`);
}

function saveQuefoods(quefood: QueFood) {
  // save ลง Database
  return http.post("/quesfoods", quefood);
}

function updateQuefoods(id: number, quefood: QueFood) {
  return http.patch("/quesfoods/" + id, quefood);
}

function deleteQuefoods(id: number) {
  return http.delete("/quesfoods/" + id);
}

function getQueueItemByStatus(text: string) {
  return http.get(`/quesfoods/status/${text}`);
}

export default {
  getQuefoods,
  saveQuefoods,
  updateQuefoods,
  deleteQuefoods,
  getQueueItemByStatus,
  getQuefoodsID,
};
