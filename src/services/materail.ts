import type Material from "@/types/Material";
import type Materail from "@/types/Material";
import http from "./axios";
function getMaterail() {
  return http.get("/material");
}

function saveMaterail(material: Material) {
  return http.post("/material", material);
}

function updateMaterail(id: number, material: Materail) {
  return http.patch(`/material/${id}`, material);
}

function deleteMaterail(id: number) {
  return http.delete("/material/" + id);
}

export default { getMaterail, saveMaterail, updateMaterail, deleteMaterail };
