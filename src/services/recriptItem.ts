import type StockItem from "@/types/StockItem";
import http from "./axios";
import type ReceiptItem from "@/types/ReceiptItem";
function getRecriptItems() {
  return http.get("/receiptsItems");
}

function saveReceiptItem(recriptItem: ReceiptItem) {
  return http.post("/receiptsItems", recriptItem);
}

function updateReceiptItem(id: number, recriptItem: ReceiptItem) {
  return http.patch(`/receiptsItems/${id}`, recriptItem);
}

function deleteReceiptItem(id: number) {
  return http.delete(`/receiptsItems/${id}`);
}

export default {
  getRecriptItems,
  saveReceiptItem,
  updateReceiptItem,
  deleteReceiptItem,
};
