import type Table from "@/Table";
import http from "./axios";

function getTables() {
  return http.get("/tables");
}

function getTablesStatus(status: string) {
  return http.get("/tables/status/" + status);
}

function saveTable(table: Table) {
  return http.post("/tables", table);
}

function updateTable(id: number, table: Table) {
  return http.patch("/tables/" + id, table);
}

function deleteTable(id: number) {
  return http.delete("/tables/" + id);
}

export default {
  getTables,
  saveTable,
  updateTable,
  deleteTable,
  getTablesStatus,
};
