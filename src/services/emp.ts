import type Emp from "@/types/Emp";
import http from "./axios";
function getEmp() {
  return http.get("/emp");
}

function saveEmp(employee: Emp) {
  return http.post("/emp", employee);
}

function updateEmp(id: number, employee: Emp) {
  return http.patch(`/emp/${id}`, employee);
}

function deleteEmp(id: number) {
  return http.delete(`/emp/${id}`);
}

export default { getEmp, saveEmp, updateEmp, deleteEmp };
