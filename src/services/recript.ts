import type Stock from "@/types/Stock";
import http from "./axios";
import type Receipt from "@/types/Receipt";
function getReceipt() {
  return http.get("/receipts");
}

function saveReceipt(receipt: Receipt) {
  return http.post("/receipts", receipt);
}

function updateReceipt(id: number, receipt: Receipt) {
  return http.patch(`/receipts/${id}`, receipt);
}

function deleteReceipt(id: number) {
  return http.delete(`/receipts/${id}`);
}

export default { getReceipt, saveReceipt, updateReceipt, deleteReceipt };
