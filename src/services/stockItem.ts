import type StockItem from "@/types/StockItem";
import http from "./axios";
function getStockItems() {
  return http.get("/stocksItems");
}

function saveStockItems(stocks: StockItem) {
  return http.post("/stocksItems", stocks);
}

function updateStockItems(id: number, stocks: StockItem) {
  return http.patch(`/stocksItems/${id}`, stocks);
}

function deleteStockItems(id: number) {
  return http.delete(`/stocksItems/${id}`);
}

export default {
  getStockItems,
  saveStockItems,
  updateStockItems,
  deleteStockItems,
};
