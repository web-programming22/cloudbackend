import type Order from "@/types/Order";
import http from "./axios";
import type OrderItem from "@/types/OrderItem";
import type Table from "@/Table";
function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: {
  orderItems: { foodId: number; amount: number }[];
  userId: number;
  tableId?: number;
}) {
  return http.post("/orders", order);
}

function saveOrderByTable(order: Order) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function updateOrderItem(
  id: number,
  order: {
    orderItems: { foodId: number; amount: number }[];
    userId: number;
    tableId?: number;
  }
) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

function getOrderByTableId(id: number) {
  return http.get(`/orders/table/${id}`);
}

export default {
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  saveOrderByTable,
  getOrderByTableId,
  updateOrderItem,
};
