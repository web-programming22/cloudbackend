import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import receriptItemsService from "@/services/recriptItem";
import receiptService from "@/services/recript";
import { ref, watch } from "vue";
import type ReceiptItem from "@/types/ReceiptItem";
import type StockItem from "@/types/StockItem";
import type Receipt from "@/types/Receipt";
import { useMaterialStore } from "./material";
import type Material from "@/types/Material";
export const useReceiptItemsStore = defineStore("ReceiptItems", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const materialStore = useMaterialStore();
  const receiptItems = ref<ReceiptItem[]>([]);

  async function getRecriptItems() {
    loadingStore.isLoading = true;
    try {
      const res = await receriptItemsService.getRecriptItems();
      receiptItems.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล ReceriptItems ได้");
    }
    loadingStore.isLoading = false;
  }

  //เเก้ไข
  const dialogReciptItem = ref(false);
  const editReceiptItem = ref<ReceiptItem>({
    name: "",
    amount: 0,
    price: 0,
    total: 0,
  });

  watch(dialogReciptItem, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editReceiptItem.value = { name: "", price: 0, amount: 0, total: 0 };
    }
  });
  function editReceiptItems(reciptItem: ReceiptItem) {
    editReceiptItem.value = JSON.parse(JSON.stringify(reciptItem));
    dialogReciptItem.value = true;
  }

  //กดปุ่มยกเลิกRecirpt
  async function CancleReceipt() {
    dialogRecipt.value = false;
    receiptItemslistPush.value = []; //เคลียลิส
    clearEditSaveReceiptItem(); // เคลียรายการของเข้า
    ClearEditSaveReceipt(); // เคลียบิลเข้าร้าน
  }

  function ClearEditSaveReceipt() {
    editSaveReceipt.value.nameEmp = "";
    editSaveReceipt.value.total = 0;
    editSaveReceipt.value.change = 0;
    editSaveReceipt.value.receive = 0;
  }

  function clearEditSaveReceiptItem() {
    editSaveReceiptItem.value.name = "";
    editSaveReceiptItem.value.price = 0;
    editSaveReceiptItem.value.amount = 0;
    editSaveReceiptItem.value.total = 0;
  }

  async function saveReceiptItem() {
    loadingStore.isLoading = true;
    try {
      if (editReceiptItem.value.id) {
        const res = await receriptItemsService.updateReceiptItem(
          editReceiptItem.value.id,
          editReceiptItem.value
        );
      } else {
        const res = await receriptItemsService.saveReceiptItem(
          editReceiptItem.value
        );
      }

      dialogReciptItem.value = false;
      await getRecriptItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล วัตถุดิบเข้าร้านได้ ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  //deleteStockItem
  async function deleteReceiptItem(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await receriptItemsService.deleteReceiptItem(id);
      await getRecriptItems();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล วัตถุดิบเข้าร้าน ได้");
    }
    loadingStore.isLoading = false;
  }

  //บันทึกวัตถุดิบเข้าร้าน
  const dialogRecipt = ref(false);
  //สิ่งที่ต้องการเเก้ไข Receipt
  const editSaveReceipt = ref<Receipt>({
    nameEmp: "",
    total: 0,
    receive: 0,
    change: 0,
    receiptItems: [],
  });

  //บันทึก receipt and receiptItem
  async function Save() {
    loadingStore.isLoading = true;
    try {
      await receiptService.saveReceipt(editSaveReceipt.value);
      await getRecriptItems();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถ บันทึกของเข้าร้าน ได้");
    }

    loadingStore.isLoading = false;
    dialogRecipt.value = false;
  }

  //บันทึก ReceiptItem
  const dialogSaveReciptItem = ref(false);
  const editSaveReceiptItem = ref<ReceiptItem>({
    name: "",
    amount: 0,
    price: 0,
  });

  //กดปุ่มเพิ่ม
  async function Push() {
    dialogSaveReciptItem.value = true;
    dialogRecipt.value = false;
  }
  //กดปุ่มยกเลิก
  async function CancleSaveItem() {
    dialogSaveReciptItem.value = false;
    dialogRecipt.value = true;
    clearEditSaveReceiptItem();
  }

  const receiptItemslistPush = ref<ReceiptItem[]>([]);
  //const materailList = ref<Material[]>([]);
  //เพิ่มลงในList
  async function SavePushList() {
    //console.log(editSaveReceiptItem.value.amount);
    // for (let i = 0; i < materialStore.material.length; i++) {
    //   materailList.value[i].name = materialStore.material[i].name;
    //   materailList.value[i].id = materialStore.material[i].id;
    //   materailList.value.push({
    //     name: materailList.value[i].name,
    //     id: materailList.value[i].id,
    //   });
    // }

    receiptItemslistPush.value.push({
      name: editSaveReceiptItem.value.name,
      amount: editSaveReceiptItem.value.amount,
      price: editSaveReceiptItem.value.price,
      total: editSaveReceiptItem.value.amount * editSaveReceiptItem.value.price,
    });

    dialogSaveReciptItem.value = false;
    dialogRecipt.value = true;
    const totalCountRecipt = ref(0);
    const count = ref(0);
    for (let i = 0; i < receiptItemslistPush.value.length; i++) {
      totalCountRecipt.value =
        receiptItemslistPush.value[i].amount *
        receiptItemslistPush.value[i].price;
      count.value = count.value + totalCountRecipt.value;
    }
    editSaveReceipt.value.total = count.value;
    //editSaveReceipt.value.change = 0;

    editSaveReceipt.value.receiptItems = receiptItemslistPush.value; // ให้ค่าใน receiptItems เป็นลิส
    clearEditSaveReceiptItem();
  }

  //ลบในลิส
  function edit(id: number) {
    receiptItemslistPush.value.splice(id, 1);
    const totalCountRecipt = ref(0);
    const count = ref(0);
    for (let i = 0; i < receiptItemslistPush.value.length; i++) {
      totalCountRecipt.value =
        receiptItemslistPush.value[i].amount *
        receiptItemslistPush.value[i].price;
      count.value = count.value + totalCountRecipt.value;
    }
    editSaveReceipt.value.total = count.value;
  }

  watch(dialogSaveReciptItem, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editSaveReceiptItem.value = { name: "", amount: 0, price: 0 };
    }
  });
  function editReceiptItemsList(reciptItem: ReceiptItem) {
    editSaveReceiptItem.value = JSON.parse(JSON.stringify(reciptItem));
    dialogSaveReciptItem.value = true;
  }

  return {
    getRecriptItems,
    loadingStore,
    messageStore,
    receiptItems,
    editReceiptItems,
    dialogReciptItem,
    editReceiptItem,
    deleteReceiptItem,
    saveReceiptItem,
    dialogRecipt,
    editSaveReceipt,
    Save,
    dialogSaveReciptItem,
    Push,
    CancleSaveItem,
    editSaveReceiptItem,
    receiptItemslistPush,
    SavePushList,
    CancleReceipt,
    edit,
    editReceiptItemsList,
  };
});
