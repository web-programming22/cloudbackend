import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { ref } from "vue";
import { useTableStore } from "./table";
import { useOrderStore } from "./order";

export const useQueFoodStore = defineStore("quecook", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const tableStore = useTableStore();
  const orderStore = useOrderStore();

  const questatus = ref<
    { id: number; food: string; status: string; tableid: string }[]
  >([]);
  const quewait = ref<
    { id: number; food: string; status: string; tableid: string }[]
  >([]);
  const quecook = ref<
    { id: number; food: string; status: string; tableid: string }[]
  >([]);
  const quefinish = ref<
    { id: number; food: string; status: string; tableid: string }[]
  >([]);

  const queserve = ref<
    { id: number; food: string; status: string; tableid: string }[]
  >([]);

  const namefoodCancle = ref("");
  const tableCancle = ref("");
  const idStatusQue = ref(0);

  async function cookingQue(id: number, namefood: string, itemid: number) {
    quecook.value.push({
      id: itemid,
      food: namefood,
      status: "ทำอาหาร",
      tableid: tableStore.editedTable.table,
    });
    quewait.value.splice(id, 1);

    //เช็คว่าในคิวcook มีเลข id เท่ากับ ในคิวstatus ไหม
    for (let i = 0; i < questatus.value.length; i++) {
      for (let j = 0; j < quecook.value.length; j++) {
        if (questatus.value[i].id === quecook.value[j].id) {
          questatus.value[i].status = "ทำอาหาร";
        }
      }
    }
  }

  async function finishQue(id: number, namefood: string, itemid: number) {
    quefinish.value.push({
      id: itemid,
      food: namefood,
      status: "เสร็จสิ้น",
      tableid: tableStore.editedTable.table,
    });
    queserve.value.push({
      id: itemid,
      food: namefood,
      status: "รอเสิร์ฟ",
      tableid: tableStore.editedTable.table,
    });
    quecook.value.splice(id, 1);
    //เช็คว่าในคิวfinish มีเลข id เท่ากับ ในคิวstatus ไหม
    for (let i = 0; i < questatus.value.length; i++) {
      for (let j = 0; j < quefinish.value.length; j++) {
        if (questatus.value[i].id === quefinish.value[j].id) {
          questatus.value[i].status = "รอเสิร์ฟ";
        }
      }
    }
  }
  async function serveQue(index: number, id: number) {
    queserve.value.splice(index, 1); // ลบตำเเหร่งที่อยู่ในคิวเสิร์ฟ
    //เช็คว่าในคิวserve มีเลข id เท่ากับ ในคิวstatus ไหม
    for (let i = 0; i < questatus.value.length; i++) {
      if (questatus.value[i].id === id) {
        questatus.value[i].status = "เสิร์ฟเเล้ว";
        return;
      }
    }
  }

  function ClearListFinish() {
    quefinish.value = [];
  }

  const iditemCancleWait = ref(0);
  const indexcancle = ref(0);
  const dialogCancleWait = ref(false);
  const DialogCancleQueWait = (
    namefood: string,
    tableId: string,
    iditem: number,
    index: number
  ) => {
    dialogCancleWait.value = true;
    namefoodCancle.value = namefood;
    tableCancle.value = tableId;
    iditemCancleWait.value = iditem; //id ตัวที่เราเลือกยกเลิกใน คิว wait
    indexcancle.value = index;
  };

  function CancleWait() {
    for (let i = 0; i < quewait.value.length; i++) {
      if (quewait.value[i].id === iditemCancleWait.value) {
        quewait.value.splice(indexcancle.value, 1);
      }
    }
    for (let i = 0; i < questatus.value.length; i++) {
      if (questatus.value[i].id === iditemCancleWait.value) {
        questatus.value[i].status = "ถูกยกเลิก";
      }
    }
    indexcancle.value = 0;
    iditemCancleWait.value = 0;
    dialogCancleWait.value = false;
  }

  const iditemCancleCook = ref(0);
  const indexcancleCook = ref(0);
  const dialogCancleCook = ref(false);

  const DialogCancleQueCook = (
    namefood: string,
    tableId: string,
    iditem: number,
    index: number
  ) => {
    dialogCancleCook.value = true;
    namefoodCancle.value = namefood;
    tableCancle.value = tableId;
    iditemCancleCook.value = iditem; //id ตัวที่เราเลือกยกเลิกใน คิว wait
    indexcancleCook.value = index;
  };

  function CancleCook() {
    for (let i = 0; i < quecook.value.length; i++) {
      if (quecook.value[i].id === iditemCancleCook.value) {
        quecook.value.splice(indexcancleCook.value, 1);
      }
    }
    for (let i = 0; i < questatus.value.length; i++) {
      if (questatus.value[i].id === iditemCancleCook.value) {
        questatus.value[i].status = "ถูกยกเลิก";
      }
    }
    indexcancleCook.value = 0;
    iditemCancleCook.value = 0;
    dialogCancleCook.value = false;
  }

  return {
    questatus,
    quewait,
    quecook,
    quefinish,
    queserve,
    CancleCook,
    CancleWait,
    ClearListFinish,
    DialogCancleQueCook,
    DialogCancleQueWait,
    cookingQue,
    dialogCancleCook,
    dialogCancleWait,
    finishQue,
    idStatusQue,
    iditemCancleCook,
    namefoodCancle,
    tableCancle,
    serveQue,
  };
});
