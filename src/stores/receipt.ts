import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Receipt from "@/types/Receipt";
import { ref, watch } from "vue";
import receriptService from "@/services/recript";

export const useReceiptStore = defineStore("Receipt", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const receipt = ref<Receipt[]>([]);

  async function getRecript() {
    loadingStore.isLoading = true;
    try {
      const res = await receriptService.getReceipt();
      receipt.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Receript ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteReceipt(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await receriptService.deleteReceipt(id);
      await getRecript();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Receript ได้");
    }
    loadingStore.isLoading = false;
  }

  const dialogRecipt = ref(false); //VueDialogListReceipt
  const editSaveReceipt = ref<Receipt>({
    nameEmp: "",
    total: 0,
    receive: 0,
    change: 0,
  });

  watch(dialogRecipt, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editSaveReceipt.value = { nameEmp: "", total: 0, receive: 0, change: 0 };
    }
  });

  function editReceipt(recipt: Receipt) {
    editSaveReceipt.value = JSON.parse(JSON.stringify(recipt));
    dialogRecipt.value = true;
  }

  return {
    loadingStore,
    messageStore,
    getRecript,
    receipt,
    deleteReceipt,
    editSaveReceipt,
    dialogRecipt,
    editReceipt,
  };
});
