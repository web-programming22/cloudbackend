import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Food from "@/types/Food";
import { useAuthStore } from "./auth";
import { useTableStore } from "./table";
import quefood from "@/services/quefood";
import { useQueFoodStore } from "./quefoods";
import type OrderItem from "@/types/OrderItem";
import type Table from "@/Table";
export const useOrderStore = defineStore("Order", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const tableStore = useTableStore();
  const queFoodStore = useQueFoodStore();

  const dialog = ref(false);
  const orders = ref<Order[]>([]);
  const editedOrder = ref<Order>({ orderItems: [] });
  const myOrder = ref<{ food: Food; amount: number; total: number }[]>([]);
  const newOrderItem = ref<OrderItem[]>([]);
  const tempOrderItem = ref<OrderItem>({
    name: "",
    price: 0,
    amount: 1,
    total: 0,
  });
  const newOrderItemId = ref(0);
  const orderId = ref(0);

  function clearOrder() {
    myOrder.value = [];
  }

  watch(dialog, (newDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedOrder.value = {};
    }
  });
  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลออเดอร์ได้");
    }
    loadingStore.isLoading = false;
  }

  /* async function openOrderByTable() {
    loadingStore.isLoading = true;

    const user: { id: number } = authStore.getUser();

    const orderItems: [] = [];

    const order = {
      tableId: tableStore.editedTable.id!,
      userId: user.id,
      orderItems: orderItems,
    };

    try {
      const res = await orderService.saveOrder(order);
      editedOrder.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล ออเดอร ได้");
      console.log(e);
    }

    console.log(editedOrder.value);

    loadingStore.isLoading = false;
  } */

  //กดคอมเฟริม
  async function openOrder() {
    loadingStore.isLoading = true;

    const user: { id: number } = authStore.getUser();

    const orderItems = myOrder.value.map(
      (item) =>
        <{ foodId: number; amount: number }>{
          foodId: item.food.id,
          amount: item.amount,
        }
    );

    const order = {
      tableId: tableStore.editedTable.id!,
      userId: user.id,
      orderItems: orderItems,
    };

    const que = {
      status: "รอเตรียม",
      orderItems: orderItems[0],
      orderItemId: 1,
    };

    /* for (let i = 0; i < myOrder.value.length; i++) {
      newOrderItem.value.push({
        name: myOrder.value[i].food.name,
        price: myOrder.value[i].food.price,
        amount: myOrder.value[i].amount,
        total: myOrder.value[i].total,
      });
    }

    editedOrder.value.amount = totalCount.value;
    editedOrder.value.total = sum.value;
    editedOrder.value.orderItems = newOrderItem.value; */

    try {
      const res = await orderService.saveOrder(order);
      editedOrder.value = res.data;
      /* const res = await orderService.updateOrder(
        editedOrder.value.id!,
        editedOrder.value
      ); // เซฟออเดอร์ */
      console.log(res.data);
      dialog.value = false;

      for (let i = 0; i < myOrder.value.length; i++) {
        if (myOrder.value[i].amount > 1) {
          for (let j = 0; j < myOrder.value[i].amount; j++) {
            queFoodStore.idStatusQue = queFoodStore.idStatusQue + 1;
            queFoodStore.questatus.push({
              id: queFoodStore.idStatusQue,
              food: myOrder.value[i].food.name,
              status: "รอเตรียม",
              tableid: tableStore.editedTable.table!,
            });
            queFoodStore.quewait.push({
              id: queFoodStore.idStatusQue,
              food: myOrder.value[i].food.name,
              status: "รอเตรียม",
              tableid: tableStore.editedTable.table!,
            });
            que.orderItemId = res.data.orderItems[i].id;
            que.orderItems = res.data.orderItems[i];
            console.log(res.data.orderItems[i]);
            const resQue = await quefood.saveQuefoods(que); // เซฟคิว
          }
        } else {
          queFoodStore.idStatusQue = queFoodStore.idStatusQue + 1;
          queFoodStore.questatus.push({
            id: queFoodStore.idStatusQue,
            food: myOrder.value[i].food.name,
            status: "รอเตรียม",
            tableid: tableStore.editedTable.table!,
          });
          queFoodStore.quewait.push({
            id: queFoodStore.idStatusQue,
            food: myOrder.value[i].food.name,
            status: "รอเตรียม",
            tableid: tableStore.editedTable.table!,
          });
          que.orderItemId = res.data.orderItems[i].id;
          que.orderItems = res.data.orderItems[i];
          //console.log(que);
          const resQue = await quefood.saveQuefoods(que); // เซฟคิว
        }
      }
      console.log(myOrder.value.length);
      clearOrder();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล ออเดอร ได้");
      console.log(e);
    }

    loadingStore.isLoading = false;
  }

  async function changeOrderTableId(oldTableId: Table, newTableId: Table) {
    loadingStore.isLoading = true;
    try {
      editedOrder.value.table = newTableId;
      updateOrder(editedOrder.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function updateOrder(order: Order) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.updateOrder(order.id!, order);
      orders.value = res.data;
    } catch (error) {
      messageStore.showError("ไม่สามารถอัพเดทข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteOrder(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.deleteOrder(id);
      await getOrders();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ ออเดอร ได้");
    }
    loadingStore.isLoading = false;
  }

  function editOrder(order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(order));
    dialog.value = true;
  }

  const sum = computed(function () {
    return myOrder.value.reduce((Sum, order) => order.total * 1 + Sum, 0);
  });

  const totalCount = computed(function () {
    return myOrder.value.reduce((Total, order) => order.amount + Total, 0);
  });

  const addOrder = async (item: Food, amount: any) => {
    for (let i = 0; i < myOrder.value.length; i++) {
      if (myOrder.value[i].food.id === item.id) {
        const amount1: number = parseInt(amount);
        myOrder.value[i].amount = myOrder.value[i].amount + amount1;
        myOrder.value[i].total = myOrder.value[i].amount * item.price;
        return;
      }
    }
    myOrder.value.push({
      food: item,
      amount: parseInt(amount),
      total: amount * item.price,
    });
  };

  const deleteMyOrder = (index: number): void => {
    myOrder.value.splice(index, 1);
  };

  return {
    orders,
    getOrders,
    dialog,
    editedOrder,
    editOrder,
    deleteOrder,
    sum,
    addOrder,
    deleteMyOrder,
    myOrder,
    totalCount,
    openOrder,
    orderId,
    //openOrderByTable,
    updateOrder,
    changeOrderTableId,
  };
});
