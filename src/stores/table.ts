import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Table from "@/Table";
import tableService from "@/services/tables";
import { useRouter } from "vue-router";
import { useOrderStore } from "./order";
import type Order from "@/types/Order";
import order from "@/services/order";
import { useLoadingStore } from "./loading";

export const useTableStore = defineStore("table", () => {
  const dialog = ref(false);
  const dialogOption = ref(false);
  const dialogCRUD = ref(false);
  const dialogChange = ref(false);
  const router = useRouter();
  const loadingStore = useLoadingStore();
  const orderStore = useOrderStore();
  const editedTable = ref<Table>({ table: "", seat: 0, status: "" });
  const changeTable = ref<Table>({ table: "", seat: 0, status: "" });

  const tables = ref<Table[]>([]);

  const tablesStatus = ref<Table[]>([]);
  const editorder = orderStore.editedOrder;

  const cleanStatus = ref<Table[]>([]);

  const url = "http://localhost:5173/menu/table=";

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getTablesStatus(status: string) {
    // get statusTable
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTablesStatus(status);
      tablesStatus.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  const saveTableClean = (table: Table, index: number) => {
    cleanStatus.value.splice(index, 1);
    editedTable.value = table;
    editedTable.value.status = "ว่าง";
    saveTable();
  };

  async function getCleanStatus(status: string) {
    // get status
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTablesStatus(status);
      cleanStatus.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  watch(dialogCRUD, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedTable.value = { table: "", seat: 0, status: "" };
    }
  });

  watch(dialogChange, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedTable.value = { table: "", seat: 0, status: "" };
      changeTable.value = { table: "", seat: 0, status: "" };
    }
  });

  const saveTable = async () => {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      } else {
        const res = await tableService.saveTable(editedTable.value);
      }
      dialogCRUD.value = false;
      await getTables();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const saveChangeTable = async () => {
    loadingStore.isLoading = true;
    try {
      if (changeTable.value.id) {
        const res = await tableService.updateTable(
          changeTable.value.id,
          changeTable.value
        );
      } else {
        const res = await tableService.saveTable(changeTable.value);
      }
      dialogChange.value = false;
      await getTables();
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const editTable = (table: Table) => {
    editedTable.value = table;
    dialogCRUD.value = true;
  };

  const deleteTable = async (id: number) => {
    try {
      const res = await tableService.deleteTable(id);
      await getTables();
    } catch (e) {
      console.log(e);
    }
  };

  const optionTable = (table: Table) => {
    editedTable.value = table;
    dialogOption.value = true;
  };

  const table = (id: number): void => {
    const index = tables.value.findIndex((item) => item.id === id);
    dialog.value = true;
  };

  const openTable = async (table: Table) => {
    loadingStore.isLoading = true;
    changeStatus(table);
    dialog.value = true;
    //await orderStore.openOrderByTable();
    loadingStore.isLoading = false;
  };

  const openShowMenu = (table: Table) => {
    router.push("/menu/" + "table=" + table.id);
    dialog.value = false;
  };

  const changeStatus = (table: Table) => {
    dialog.value = true;
    dialogOption.value = false;
    editedTable.value = table;
    editedTable.value.status = "ไม่ว่าง";
    saveTable();
  };

  const selectTable = (table: Table) => {
    changeTable.value = table;
  };

  const confirmTable = () => {
    const eStatus = editedTable.value.status;
    const cStatus = changeTable.value.status;

    orderStore.changeOrderTableId(editedTable.value, changeTable.value);

    changeTable.value.status = eStatus;
    editedTable.value.status = "รอทำความสะอาด";

    saveTable();
    saveChangeTable();

    dialogChange.value = false;
  };

  return {
    tables,
    dialog,
    table,
    getTables,
    dialogOption,
    editedTable,
    optionTable,
    dialogCRUD,
    saveTable,
    editTable,
    deleteTable,
    dialogChange,
    changeStatus,
    selectTable,
    changeTable,
    saveChangeTable,
    confirmTable,
    openTable,
    openShowMenu,
    getTablesStatus,
    tablesStatus,
    cleanStatus,
    getCleanStatus,
    saveTableClean,
    url,
  };
});
