import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

import stockItemsService from "@/services/stockItem";
import stockService from "@/services/stock";
import type StockItem from "@/types/StockItem";
import { ref, watch } from "vue";
import type Stock from "@/types/Stock";
export const useStockItemsStore = defineStore("StockItems", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const stockItems = ref<StockItem[]>([]);

  //getStock
  async function getStockItems() {
    loadingStore.isLoading = true;
    try {
      const res = await stockItemsService.getStockItems();
      stockItems.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล StockItems ได้");
    }
    loadingStore.isLoading = false;
  }

  //บันทึกสต็อก
  const SaveStockList = ref<StockItem>();
  const dialogStock = ref(false);
  //สิ่งที่ต้องการเเก้ไข StockItem
  const editSaveStockItem = ref<StockItem>({
    amount: 0,
    price: 0,
  });

  //สิ่งที่ต้องการเเก้ไข Stock
  const editSaveStock = ref<Stock>({
    nameEmp: "",
  });

  //dialog บันทึก
  watch(dialogStock, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editSaveStockItem.value = { amount: 0, price: 0 };
    }
  });

  //บันทึก stock stockitem
  async function Save() {
    loadingStore.isLoading = true;
    try {
      await stockService.saveStock(editSaveStock.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก รายการสต็อก ได้");
    }
    loadingStore.isLoading = false;
    dialogStock.value = false;
  }

  //Dialog เเก้ไข
  const dialogStockItem = ref(false);

  const editStockItem = ref<StockItem>({
    nameMaterial: "",
    amount: 0,
    price: 0,
  });

  watch(dialogStockItem, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editStockItem.value = { nameMaterial: "", amount: 0, price: 0 };
    }
  });
  function editStockItems(stockItem: StockItem) {
    editStockItem.value = JSON.parse(JSON.stringify(stockItem));
    dialogStockItem.value = true;
  }

  async function saveStockItem() {
    loadingStore.isLoading = true;
    try {
      if (editStockItem.value.id) {
        const res = await stockItemsService.updateStockItems(
          editStockItem.value.id,
          editStockItem.value
        );
      } else {
        const res = await stockItemsService.saveStockItems(editStockItem.value);
      }

      dialogStockItem.value = false;
      await getStockItems();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล วัตถุดิบ ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  //deleteStockItem
  async function deleteStockItem(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await stockItemsService.deleteStockItems(id);
      await getStockItems();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล วัตถุดิบ ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    getStockItems,
    loadingStore,
    messageStore,
    stockItems,
    dialogStock,
    editSaveStock,
    editSaveStockItem,
    SaveStockList,
    Save,
    dialogStockItem,
    editStockItem,
    editStockItems,
    saveStockItem,
    deleteStockItem,
  };
});
