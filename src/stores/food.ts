import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Food from "@/types/Food";
import foodService from "@/services/food";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useFoodStore = defineStore("Food", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const category = ref(0);

  // const myOrder = ref<Food[]>([]);

  const foods = ref<Food[]>([]);
  const editedFood = ref<Food & { files: File[] }>({
    name: "",
    price: 0,
    categoryId: 1,
    image: "no_img_available.jpg",
    files: [],
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedFood.value = {
        name: "",
        price: 0,
        categoryId: 1,
        image: "no_img_available.jpg",
        files: [],
      };
    }
  });

  watch(category, async (newCategory, oldCategory) => {
    await getFoodsByCategory(newCategory);
  });

  async function getFoodsByCategory(category: number) {
    loadingStore.isLoading = true;
    if (category === 0) {
      getFoods();
      return;
    }
    try {
      const res = await foodService.getFoodsByCategory(category);
      foods.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getFoods() {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.getFoods();
      foods.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveFood() {
    loadingStore.isLoading = true;
    try {
      if (editedFood.value.id) {
        const res = await foodService.updateFood(
          editedFood.value.id,
          editedFood.value
        );
      } else {
        const res = await foodService.saveFood(editedFood.value);
      }

      dialog.value = false;
      await getFoods();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Food ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteFood(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.deleteFood(id);
      await getFoods();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Food ได้");
    }
    loadingStore.isLoading = false;
  }
  function editFood(food: Food) {
    editedFood.value = JSON.parse(JSON.stringify(food));
    dialog.value = true;
  }

  // const addOrder = (id: number): void => {
  //   const index = foods.value.findIndex((item) => item.id === id);
  //   myOrder.value.push(foods.value[index]);
  // };

  return {
    foods,
    getFoods,
    deleteFood,
    editFood,
    saveFood,
    editedFood,
    dialog,
    category,
    getFoodsByCategory,
  };
});
