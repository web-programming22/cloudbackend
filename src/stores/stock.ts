import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Stock from "@/types/Stock";
import stockService from "@/services/stock";
export const useStockStore = defineStore("Stock", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  const stock = ref<Stock[]>([]);
  const editedStock = ref<Stock>({ nameEmp: "" });

  //dialog เเก้ไข name
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStock.value = { nameEmp: "" };
    }
  });

  //getStock
  async function getStock() {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.getStock();
      stock.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
    }
    loadingStore.isLoading = false;
  }

  //saveStock
  async function saveStock() {
    loadingStore.isLoading = true;
    try {
      if (editedStock.value.id) {
        const res = await stockService.updateStock(
          editedStock.value.id,
          editedStock.value
        );
      } else {
        const res = await stockService.saveStock(editedStock.value);
      }

      dialog.value = false;
      await getStock();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Stock ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  //deleteStock
  async function deleteStock(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.deleteStock(id);
      await getStock();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Stock ได้");
    }
    loadingStore.isLoading = false;
  }
  function editStock(stock: Stock) {
    editedStock.value = JSON.parse(JSON.stringify(stock));
    dialog.value = true;
  }

  return {
    stock,
    getStock,
    dialog,
    editedStock,
    editStock,
    saveStock,
    deleteStock,
  };
});
