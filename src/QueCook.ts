export default interface QueCook {
  id: number;
  menu: String;
  table: number;
  status: string;
}
